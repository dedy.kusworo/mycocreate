package model
import (
	// "fmt"
	"be-6/app/utils"
	"github.com/dgrijalva/jwt-go"
	"github.com/pkg/errors"
	"gorm.io/gorm"
)


type Users struct {
	IdUser             int64  `json:"id_user" orm:"id_user"`
	NameUser           string `json:"name" orm:"name_user"`
	IdTypeUser         int64  `json:"type_user" orm:"id_type_user"`
	Username           string `json:"username" orm:"username"`
	Email              string `json:"email" orm:"email"`
	Password           string `json:"password" orm:"password"`
	Birth              int64  `json:"birth" orm:"birth"`
	Gender             string `json:"gender" orm:"gender"` // male/female
	Phone              string `json:"phone" orm:"phone"`
	Address            string `json:"address" orm:"address"`
	PostCode           int64  `json:"postcode" orm:"post_code"`
	ShortBio           int64  `json:"short_bio" orm:"short_bio"`
	StatusVerification string `json:"verification" orm:"status_verification"`
	LoginType          string `json:"login_type" orm:"login_type"`
}

type Auth struct {
	Email string `json:"email"`
	Password string `json:"password"`
}


func Login(auth Auth) (bool,error,string){
	var user Users
	if err := DB.Where(&Users{Email: auth.Email}).First(&user).Error; err!=nil{
		if err == gorm.ErrRecordNotFound{
			return false,errors.Errorf("User not found"),""
		}
	}

	err := utils.HashComparator([]byte(user.Password),[]byte(auth.Password))
	if err != nil{
		return false, errors.Errorf("Incorrect Password"),""
	} else {

		sign := jwt.NewWithClaims(jwt.SigningMethodHS256,jwt.MapClaims{
			"email":auth.Email,
			"id_user":user.IdUser,
		})

		token ,err := sign.SignedString([]byte("secret"))
		if err != nil {
			return false,err,""
		}
		return true,nil,token
	}
}


func InsertNewUser(user Users) (bool,error){
	var nilaiCek int64
	user.IdTypeUser = 2
	user.StatusVerification = "False"
	user.LoginType = "on website"
	err :=DB.Model(&user).Where("name_user =?", user.NameUser).Count(&nilaiCek)
	if err != nil {
		if(nilaiCek == 0){
			if err := DB.Create(&user).Error;err!=nil{
				return false, errors.Errorf("invalid prepare statement :%+v\n", err)
			}
			return true, errors.Errorf("invalid prepare statement :%+v\n", err)
		}
	 	return false, errors.Errorf("invalid prepare statement :%+v\n", err)
	}else{
		return false,nil
	}
}


func GetAllUser() (bool,error, []Users){
	var user []Users
	if err := DB.Find(&user).Error;err!=nil{
		if err == gorm.ErrRecordNotFound{
			return false,errors.Errorf("User not found"),[]Users{}
		} else {
			return false, errors.Errorf("invalid prepare statement :%+v\n", err), []Users{}
		}
	}

	return true,nil,user
}