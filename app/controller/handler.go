package controller

import (
	"be-6/app/model"
	"be-6/app/utils"
	"github.com/gin-gonic/gin"
	"log"
	"net/http"
)


func GetAllUser(c *gin.Context){
	log.Println("Get All User")
	flag,err,usr := model.GetAllUser();if err != nil {
		utils.WrapAPIError(c,err.Error(),http.StatusInternalServerError)
		return
	}
	if flag{
		utils.WrapAPIData(c,map[string]interface{}{
			"users":usr,
		},http.StatusOK,"success")
		return
	}
}


// Version 2


func CreateUser (c *gin.Context){

	var user model.Users
	if err := c.Bind(&user); err!= nil {
		utils.WrapAPIError(c,err.Error(),http.StatusBadRequest)
		return
	}
	pass, err := utils.HashGenerator(user.Password); if err != nil{
		utils.WrapAPIError(c,err.Error(),http.StatusBadRequest)
		return
	}
	user.Password = pass
	flag,err := model.InsertNewUser(user)
	if flag{
		utils.WrapAPISuccess(c,"success",http.StatusOK)
		return
	}else {
		utils.WrapAPIError(c,err.Error(),http.StatusBadRequest)
		return
	}
}


func Login(c *gin.Context){
	var auth model.Auth
	if err := c.Bind(&auth); err != nil {
		utils.WrapAPIError(c,err.Error(),http.StatusBadRequest)
		return
	}
	log.Println("LOGIN")
	flag,err,token := model.Login(auth); if flag{
		utils.WrapAPIData(c,map[string]interface{}{
			"token":token,
		},http.StatusOK,"success")
	} else {
		utils.WrapAPIError(c,err.Error(),http.StatusBadRequest)
	}
}
