FROM golang:1.14 AS builder

WORKDIR /go/src/app

COPY . .

ENV GO111MODULE=on\
    CGO_ENABLED=0\
    GOOS=linux\
    GOARCH=amd64

RUN go get -d -v ./...

RUN go install -v ./...

RUN go build -o main .

FROM scratch

WORKDIR /dist

COPY . .

COPY --from=builder /go/src/app/main .

EXPOSE 8081

CMD ["./main"]



# FROM golang:alpine AS builder

# ENV GO111MODULE=on \
#     CGO_ENABLED=0 \
#     GOOS=linux \
#     GOARCH=amd64

# WORKDIR /build

# COPY go.mod .
# COPY go.sum .

# RUN go mod download

# COPY . .

# RUN go build -o main .

# WORKDIR /dist

# RUN cp /build/main .

# FROM scratch

# COPY --from=builder /dist/main /

# CMD ["/main"]
