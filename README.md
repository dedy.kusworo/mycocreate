# BE-6

## cara menggunakan repo

- nama database mysql : db_dgt_mvc
- nama server : server1mvc.exe
- ip address AWS EC2 : http://54.158.203.226
- listen port : 80
- contoh method : GET http://54.158.203.226:80/api/v1/users

---

## menggunakan AWS EC2
<details>
<summary>"Lihat AWS EC2"</summary>

1. masuk ke akun AWS masing2 EC2
2. setting server
3. copy public ip
4. masuk ke putty atau di cmd C:\Users\asus>`ssh -i mvc.pem ubuntu@ec2-34-236-143-56.compute-1.amazonaws.com` taruh dlu file .pem di asus
5. install MYSQL di server AWS
```
sudo apt-get update 
sudo apt install mysql-server
sudo mysql -u root (masuk ke mysql)
ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'root';
FLUSH PRIVILEGES;
```
- keluar dari mysql
- masuk lg dgn `sudo mysql -u root -p`
- masukkan password root
- buat database `create database db_dgt_mvc`
- gunain db `use db_dgt_mvc`
- lihat isi table dengan `select * from users`

---
>> untuk memulai berulan2g kembali esok hari atau kapanpun,
6. jalankan server AWS `ssh -i mvc.pem ubuntu@ec2-54-158-203-226.compute-1.amazonaws.com`
7. jalan kan mysql
8. jalankan `./server1mvc.exe`
9. jalankan method postman

</details>

---

## deploy ci/cd
- setiap melakukan deploy, matikan dulu server AWS yang sedang berjalan. baru deploy akan berhasil
<details>
<summary>"Lihat Deploy"</summary>
![deploy](/uploads/fae447af71c51300fbe39ee49a413375/deploy.JPG)

</details>

---

## load balancer
- install apache dulu di aws (tutorial)[https://www.digitalocean.com/community/tutorials/how-to-install-the-apache-web-server-on-ubuntu-20-04]
- jalankan sampai step 3. jangan lupa allow `sudo ufw allow 'Apache'`

## postman
https://documenter.getpostman.com/view/13070406/TVene8C6

## menggunakan DOCKER (belum sambung ke AWS)

<details>
<summary>"Lihat Docker"</summary>

1. gunain mysql
```
docker pull mysql
docker run --name db_dgt_mvc -e MYSQL_ROOT_PASSWORD=root -d mysql
```
2. lihat default port mysql
```
docker container ls
docker container ps
```
3. buat network `digitalentbank` - jika belum ada, jalankan `docker network ls`
```
docker network create --gateway 172.18.0.1 --subnet 172.18.0.0/24 digitalentbank
```
4. cara build docker image
```
docker-compose up
```
5. kemudian buka cli mysql bawah.. bikin database `create database db_dgt_mvc` dan restart apinya di docker.
![docker](/uploads/adcec8b3a6d5194c86d56bcbb9823c1d/docker.JPG)

</details>

---

byobu semangat!
