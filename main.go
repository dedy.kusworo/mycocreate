package main

import (
	"be-6/app/controller"
	// "be-6/app/middleware"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

func setupServer() *gin.Engine {
	router := gin.Default()

	cfg := cors.DefaultConfig()
	cfg.AllowAllOrigins = true
	cfg.AllowCredentials = true
	cfg.AllowMethods = []string{"GET","POST"}
	cfg.AllowHeaders = []string{"Authorization","Origin","Accept","X-Requested-With"," Content-Type", "Access-Control-Request-Method", "Access-Control-Request-Headers"}
	router.Use(cors.New(cfg))

	router.POST("/api/v1/user/add", controller.CreateUser)
	router.POST("/api/v1/login",controller.Login)
	router.GET("/api/v1/users",controller.GetAllUser)

	return router
}

func main(){	
	setupServer().Run(":80")
}